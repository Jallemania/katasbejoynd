﻿

namespace BejoyndKatas._2024;

public static class Generations
{
    public static bool RunCases()
    {
        var success = true;

        List<(int removed, string gender, string answer)> cases =
        [
            (-3, "m", "great grandfather"),
            (1, "f", "daughter"),
            (-3, "f", "great grandmother"),
            (-2, "m", "grandfather"),
            (-2, "f", "grandmother"),
            (-1, "m", "father"),
            (-1, "f", "mother"),
            (0, "f", "me!"),
            (1, "m", "son"),
            (1, "f", "daughter"),
            (2, "m", "grandson"),
            (2, "f", "granddaughter"),
            (3, "m", "great grandson"),
            (3, "f", "great granddaughter"),
            (0, "m", "me!")
        ];

        foreach (var item in cases)
        {
            if (!Test.Assert(Generation(item.removed, item.gender), item.answer))
            {
                success = false;
            }
        }

        return success;
    }

    private static string Generation(int removed, string gender)
    {
        if (gender != "m" || gender != "f") return "Not a valid Gender.";
        
        return removed switch
        {
            -3 => gender == "m" ? "great grandfather" : "great grandmother",
            -2 => gender == "m" ? "grandfather" : "grandmother",
            -1 => gender == "m" ? "father" : "mother",
            0 => "me!",
            1 => gender == "m" ? "son" : "daughter",
            2 => gender == "m" ? "grandson" : "granddaughter",
            3 => gender == "m" ? "great grandson" : "great granddaughter",
            _ => "Not a valid generation."
        };
    }
}