﻿
using System.Text.RegularExpressions;

namespace BejoyndKatas._2024;

public static class ValidHex
{
    public static void RunCases()
    {
        (string Case, bool Result)[] testCases =
            [
                ("#CD5C5C", true),
                ("#EAECEE", true),
                ("#eaecee", true),
                ("#CD5C58C", false),
                ("#CD5C5Z", false),
                ("#CD5C&C", false),
                ("CD5C5C", false),
                ("#123CCCD", false),
                ("#123456", true),
                ("#987654", true),
                ("#9876543", false),
                ("#CCCCCC", true),
                ("#ZCCZCC", false),
                ("#Z88Z99", false),
                ("#Z88!99", false),
            ];

        var validHexRegExp = new Regex("^#(?:[0-9a-fA-F]{3}){1,2}$");

        foreach (var test in testCases)
        {
            var success = validHexRegExp.IsMatch(test.Case);

            Console.WriteLine(success == test.Result ? "success" : "fail...");  
        }
    }
}
